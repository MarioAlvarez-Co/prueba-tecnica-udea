import {useState} from 'react';

const initialState={
    cart:[],
    toggle:false,
    search:'',
    products:[]
}

    
 export const useInitialState=()=>{    
    const [state,setState]=useState(initialState)
    const setProducts=(products)=>{
        setState({
            ...state,
            products:products
        })
    }
    const setSearch=(search)=>{
        setState({
            ...state,
            search:search
        })
    }
    const changeToggle=()=>{
        setState({
            ...state,
            toggle:!state.toggle
        })
    }
    const addToCart=(payload)=>{        
       if(state.cart.includes(payload)){
        const index=state.cart.indexOf(payload)
        let newItem=state.cart[index]
        newItem.cantidad+=1;
        let newCart=state.cart
        newCart[index]=newItem
        setState({
            ...state,
            cart:newCart
        })
       }else{
           setState({
               ...state,
               cart:[...state.cart,{...payload,cantidad:1}]
           })
       }
    }
    const removeFromCart=(payload)=>{
        setState({
            ...state,
            cart:state.cart.filter(items=>items.id!==payload.id)
        });
    }
    return{
        state,
        addToCart,
        removeFromCart,
        changeToggle, 
        setSearch,
        setProducts       
    }
}