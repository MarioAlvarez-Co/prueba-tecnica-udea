import React,{useContext} from 'react'
import { useNavigate } from 'react-router-dom';
import AppContext from '../context/AppContext';
import'../styles/ProductItem.css'
const ProductItem = ({info}) => {
  const {addToCart,state,changeToggle }=useContext(AppContext)
  const navigate = useNavigate() ;  
  const moreDetails =()=>{
    navigate(`/ProductInfo/${info.id}/${info.title}`)
    if (state.toggle===true) {
      changeToggle()
    }
  }
  const handleItem=()=>{
    addToCart(info)
  }
  function formatCurrency(value){    
    const formatterPeso = new Intl.NumberFormat( {
       style: 'currency',
       currency: 'COP',
       minimumFractionDigits: 0
     })
     return(formatterPeso.format(value))
     
     

  }
   
  return (    
    <div className='Product-Item'>      
      <div className='data-product' onClick={moreDetails}>
        <div className='title-img'>
          <img src={info.thumbnail} alt="" />        
          <h2>{info.title}</h2>
        </div> 
        <div className='price'>
          <div>
            <h4>Precio</h4> 
            <h3>{formatCurrency(info.price)}  {info.currency_id}</h3>                  
          </div>
            <h4>disponibles :{info.available_quantity}</h4>                           
          </div>  
      </div>
      <div className='button-add-cart'>

        <button onClick={handleItem}>añadir al Carrito</button>
      </div>
    </div>
  )
}

export default ProductItem ;