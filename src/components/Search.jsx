import React,{ useState,useContext } from 'react'
import {  useNavigate} from "react-router-dom";
import AppContext from '../context/AppContext';
import '../styles/Search.css'

const Search = () => {   
  const navigate = useNavigate()   
  const {state,setSearch,changeToggle}= useContext(AppContext);
  const [keyword,setKeyword]=useState('')     

  const handleSubmit=(event)=>{
      event.preventDefault()   
      setSearch(keyword.replace(' ',''))      
      navigate(`/search/${keyword.replace(' ','')}`) 
      if (state.toggle===true) {
        changeToggle()
      }
    } 
    const handleChange=(event)=>{
      setKeyword(event.target.value)           
    }               
  return (
   <form onSubmit={handleSubmit} >
        <input className='input-text' onChange={handleChange} value={keyword}type="text" placeholder='Encuentra lo que buscas'/>
        <input className='input-buton' onClick={handleSubmit}  type="button"/>
   </form>
  )
}

export default Search