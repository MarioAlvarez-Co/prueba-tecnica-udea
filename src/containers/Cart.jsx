import React,{useContext} from 'react'
import AppContext from '../context/AppContext';
import ProductItem from '../components/ProductItem'
import'../styles/Cart.css';
import { v4 as uuidv4 } from 'uuid';
const Cart = () => {
  const {state,changeToggle,removeFromCart}=useContext(AppContext)
  const productsCart=state.cart
  function remove(itemId){
    removeFromCart(itemId)
  }
  return (
    
    <div className='Cart'>
      <div className='head-cart'>
        <div className='head-cart-info'>
          <h2>Cart</h2>            
          <p>{productsCart.length}</p>
          
        </div>
        <p className='close-cart' onClick={changeToggle}>x</p>
      </div>
      <div className='cart-Products'>
      {productsCart.map(element=>(            
          <ProductItem key={element.id}  info={element}/>                                         
      ))}    
      </div>
    </div>
  )
}

export default Cart