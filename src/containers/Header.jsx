import React ,{useContext}from 'react'
import { useNavigate } from 'react-router-dom'
import Search from '../components/Search'
import Cart from './Cart'
import '../styles/Header.css'
import img from '../assets/market_2131.png'
import cart from'../assets/cart-Icon.png'
import AppContext from '../context/AppContext'
const Header = () => {  
  const {state,changeToggle} = useContext(AppContext);
  const navigate=useNavigate()
  function goHome(){
    navigate(`/`)
  }
  return (
    <header className='Header'>
      <div className='title-and-Search'>   
        <div className='logo-title'>
          <img  className='logo-img' src={img}  alt="" />
          <h1 onClick={goHome}>Libre-Store</h1>        
        </div>   
        <div className='search-container'>
          <Search/>
        </div>      
      </div>
      
      <div className='cart-container'>
        <p className='cart-counter'>{state.cart.length}</p>
        <img  onClick={changeToggle}  src={cart} alt="" width={'64px'} height={'64px'}/> 
        {state.toggle&&<Cart/> }
      </div>
    </header>
  )
}

export default Header