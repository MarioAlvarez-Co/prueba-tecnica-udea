import React ,{useContext}from 'react'
import { useParams } from 'react-router-dom';
import ProductItem from '../components/ProductItem';
import AppContext from '../context/AppContext';
import { useGetApiInfo } from '../Hooks/useGetApiInfo'
import'../styles/ProductList.css'
const ProductList = () => {   
  const {state}=useContext(AppContext);
  const {useSearchProducts}=useGetApiInfo();               
  const searchProducts=useSearchProducts(state.search===''?useParams().search:state.search)                    
  return (
    <div className='Product-List'>      
      {searchProducts.map(element=>(
        <ProductItem key={element.id}  info={element}/>
      ))}                
    </div>
  )
}

export default ProductList