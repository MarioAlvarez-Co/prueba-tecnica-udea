import React ,{useContext}from 'react'
import'../styles/Home.css'
import {useNavigate} from 'react-router-dom'
import AppContext from '../context/AppContext';


const Home = () => {   
    const navigate=useNavigate();
    const {setSearch,state,changeToggle}= useContext(AppContext);
    function handleSearch (keyword){
        setSearch(keyword)
        navigate(`/search/${keyword}`)   
        if (state.toggle===true) {
            changeToggle()
          }                     
    }
    return (
        <div className='Home'>                     
        
            <img onClick={()=>{handleSearch('ofertas')}} src="https://http2.mlstatic.com/storage/splinter-admin/o:f_webp,q_auto:best/1660089920357-home-sliderdesktop-2.jpg" alt="" />
                 
          <img onClick={()=>{handleSearch('audio')}} src="https://http2.mlstatic.com/D_NQ_793616-MLA51095704307_082022-OO.webp" alt="" />
                    
          <img onClick={()=>{handleSearch('importado')}} src="https://http2.mlstatic.com/D_NQ_659749-MLA51163696710_082022-OO.webp" alt="" />
                                               
        </div>
    )
}

export default Home