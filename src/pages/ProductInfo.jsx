import React from 'react'
import { useParams } from 'react-router-dom'
import { useGetApiInfo } from '../Hooks/useGetApiInfo'
import'../styles/ProductInfo.css'
const ProductInfo = () => {
  const {useSearchItem}=useGetApiInfo()
  const productInfo=useSearchItem(useParams().id)
  function formatCurrency(value){    
    const formatterPeso = new Intl.NumberFormat( {
       style: 'currency',
       currency: 'COP',
       minimumFractionDigits: 0
     })
     return(formatterPeso.format(value))          
  }      
  return (
    <div className='ProductInfo'>
      <div className='img'>  
        <img src={`https://http2.mlstatic.com/D_${productInfo.thumbnail_id}-O.jpg`} alt="" />
      </div>   
      <div className='info'>
        <h2>{productInfo.title}</h2>        
        <h2> ${formatCurrency(productInfo.price)}</h2>        
        <p>{productInfo.warranty==null?"sin garantia":productInfo.warranty}</p>  

        <h2>Caracteristicas</h2>
        <ul>                    
        </ul>                                
      </div>       
    </div>
  )
}
export default ProductInfo