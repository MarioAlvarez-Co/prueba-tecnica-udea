import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from '../pages/Home';
import ProductInfo from'../pages/ProductInfo'
import AppContext from '../context/AppContext';
import { useInitialState } from '../Hooks/useInitialState';
import Header from '../containers/Header';
import ProductList from '../containers/ProductList';
import '../styles/App.css'
function App() {  
  const initialState=useInitialState()
  return (
    <AppContext.Provider value={initialState} >
      <div className="App">
        <BrowserRouter >
          <Header/>
          <Routes>                      
          <Route exact path='*' element={<Home/>}/>          
          <Route exact path='search/:search' element={<ProductList/>}/>          
          <Route exact path="/ProductInfo/:id/*" element={<ProductInfo/>} />
          </Routes>
        </BrowserRouter>
        
      </div>
    </AppContext.Provider>
  );
}

export default App;



